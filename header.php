<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 */
 
global $post, $wp_query;
$carecru_header_type = get_post_meta( isset( $post->ID ) ? $post->ID : (isset($wp_query->post) && isset($wp_query->post->ID) ? $wp_query->post->ID : 0), 'carecru_header_type', true );
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>
<body <?php body_class('carecru-wrapper'); ?>>
<header id="Header">
    <nav id="MainMenu" class="header-<?php echo $carecru_header_type ? $carecru_header_type : 'standard'; ?>">
        <div class="container">
        	<div class="row">
                <div class="col-4 logo">
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' ); ?>" rel="index" data-index="home"><?php
                    switch($carecru_header_type)
                    {
                        case 'standard':
                        default:
                            $custom_logo_id = get_theme_mod( 'custom_logo' );
                            break;
                        case 'sticky':
                            $custom_logo_id = attachment_url_to_postid(get_theme_mod( 'sticky_logo', 0 ));
                            break;
                        case 'sticky-white':
                            $custom_logo_id = attachment_url_to_postid(get_theme_mod( 'white_logo', 0 ));
                            break;
                    }
                    $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                    if ( has_custom_logo() || $logo ) {
                            $logo_origin = false;
							
                        if(has_custom_logo())
                            $logo_origin = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
                        
                        $theme_white_logo = attachment_url_to_postid(get_theme_mod( 'white_logo', 0 ));
                        $white_logo = wp_get_attachment_image_src( $theme_white_logo , 'full' );    
                        
						if($carecru_header_type === 'sticky')
						{
							echo '<img src="' . esc_url( $white_logo[0] ) . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" class="img-fluid logo-mobile" >';
						}
                        
                        echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" class="img-fluid" data-sticky="' . ($white_logo && isset($white_logo[0]) ? esc_url( $white_logo[0] ) : '') . '" data-src="' . esc_url( $logo[0] ) . '">';
                    } else {
                        echo '<h3>'. get_bloginfo( 'name' ) .'</h3>';
                    }
                    ?></a>
                </div>
                <div class="col-8 navbar">
                	<button type="button" class="navbar-toggler" aria-open="false">
                    	<span class="navbar-toggler-icon">&nbsp;</span>
                        <span class="navbar-toggler-icon">&nbsp;</span>
                        <span class="navbar-toggler-icon">&nbsp;</span>
                    </button>
                    <?php
                        if ( has_nav_menu( 'primary' ) ) {
                            wp_nav_menu(array(
                                'theme_location'	=> 'primary',
                                'menu_class'		=> 'navbar-menu',
                                'menu_id'			=> '',
                                'echo'				=> true,
                                'items_wrap'		=> '<ul class="%2$s">%3$s</ul>',
                                'depth'				=> 2,
                            ));
                        }
                    ?>
                </div>
            </div>
        </div>
    </nav>
</header>