<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * Single Post
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/

 // We need breadcrumb here
include CARECRU_INC . '/bootstrap-breadcrumb.php';

// Let's get all we need
$queried_object = get_queried_object();
$current_category = isset($queried_object->term_id) ? $queried_object->term_id : 0;
$current_parent = 0;

$category_array = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false
) );

$category = array();
foreach($category_array as $key => $obj){
    $category[$obj->parent][] = $obj;

    if($obj->term_id === $current_category) $current_parent = $obj->parent;
}

get_header();
?>
<section class="container-fluid pb-5" id="SinglePost">
    <div class="container">
        <div class="row">
            <?php while ( have_posts() ) : the_post(); $article_id = get_the_ID(); ?>
            <div class="col-lg-9">
                <div class="row no-gutters align-items-center justify-content-center meta-container">
                    <div class="col-md-5 col-lg-6 desktop"><?php bootstrap_breadcrumb(); ?></div>
                    <div class="col-md-3 col-lg-3 desktop">
                        <div class="meta"><i class="far fa-clock"></i> <?php echo get_the_date('F d, Y'); ?></div>
                    </div>
                    <div class="col-md-4 col-lg-3 desktop">
                        <div class="meta"><i class="fas fa-user"></i> <?php echo get_the_author_meta('display_name'); ?></div>
                    </div>
                    <div class="col-12 mobile"><?php get_template_part( 'includes/menu', 'sections' ); ?></div>
                </div>

                <div class="row">
                    <h1 class="col-12">
                        <?php the_title(); ?>
                    </h1>
                    <div class="col-12 thumbnail">
                    <?php echo get_the_post_thumbnail( $article_id, 'large', array( 'class' => 'card-img-top img-fluid', 'alt' => esc_attr(get_the_title()) ) ); ?>
                    </div>
                    <div class="col-12 mobile">
                        <span class="meta"><i class="far fa-clock"></i> <?php echo get_the_date('F d, Y'); ?></span> 
                        <span class="meta"><i class="fas fa-user"></i> <?php echo get_the_author_meta('display_name'); ?></span>
                    </div>
                    <div class="col-12 mt-3">
                        <?php the_content(); ?>
                    </div>
                    <div class="col-12 mb-5">
                        <?php the_tags('<div class="tags">','', '</div>'); ?>
                    </div>
                    <div class="col-6 mb-3 text-left pn">
                        <?php previous_post_link('%link', '<i class="fa fa-arrow-left" aria-hidden="true"></i> '.__('Previous Post','carecru')); ?>
                    </div>
                    <div class="col-6 mb-3 text-right pn">
                        <?php next_post_link('%link', __('Next Post','carecru').' <i class="fa fa-arrow-right" aria-hidden="true"></i>'); ?>
                    </div>
                </div>

                <hr>
                <h3 class="mt-5"><?php _e('RELATED POSTS', 'carecru'); ?></h3>
                <div class="row">
                    <?php
                        $args = array(
                            'post_type'			=> 'post',
                            'post_status'	    => 'publish',
                            'posts_per_page'	=> 2,
                            'post__not_in' 		=> array($article_id),
                            'ignore_sticky_posts'	=> 1,
                            'orderby' => 'rand'
                        );
                        $post_categories = wp_get_post_categories($article_id);
                        if($post_categories)
                        {
                            $args['category__in'] = $post_categories;
                        }
                        $loop = new WP_Query( $args );
                        if($loop->have_posts()) : 
                            $i = 0; $realated = true;
                            while ( $loop->have_posts() ) : $loop->the_post();
                                $article_id = get_the_ID();
                                get_template_part( 'includes/loop', 'posts' );
                                ++$i;
                            endwhile;
                            wp_reset_query();
                        endif;
                    ?>
                </div>
                <h3 class="mt-5"><?php _e('LEAVE A COMMENT', 'carecru'); ?></h3>
                <div class="card bg-light">
                    <div class="card-body">
                        <?php comments_template(); ?>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <div class="col-lg-3 widget-area">
                <?php if(is_dynamic_sidebar('blog-sidebar') && is_active_sidebar('blog-sidebar')): ?>
                    <?php dynamic_sidebar('blog-sidebar'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer();