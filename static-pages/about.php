<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");

/**********************************

 * Template Name: About Page

 *

 * @package WordPress

 * @subpackage carecru

 * @since 0.0.1

 * @version 0.0.1

 * @author Ivijan-Stefan Stipic

 * @url https://infinitumform.com

 **********************************/



// Add separated file for this page

add_action('wp_enqueue_scripts', 'carecru_about_css', 1);

function carecru_about_css() {

    wp_enqueue_style('carecru-home', CARECRU_CSS . '/about.css', array('carecru-general'), CARECRU_VERSION);

}

add_filter( 'body_class', function( $classes ) {
    return array_merge( $classes, array( 'about-page' ) );
} );

get_header();

?>


<section class="container-fluid about-header">
    <div class="header-overlay"></div>
    <div class="row">
        <div class="col-lg-12">
            <h1>About Us</h1>
            <p>We grow, measure and manage dental practices without human intervention</p>
        </div>
    </div>
</section>

<section class="container-fluid about-info">
    <div class="container about-info-wrapper">
        <div class="row justify-content-between about-info-container">
            <div class="col-lg-6">
                <h2>Care Cru is the creator of <span>Donna™</span></h2>
                <p>A virtual dental practice assistant, powered by AI, that grows and manages dental practices. Through Donna, group and private dental practices can grow and more effectively manage their business while drastically enhancing the patient experience.</p>
            </div>
            <div class="col-lg-6 d-flex flex-column justify-content-center">
                <p>The culture? We are an ambitious team of no-nonsense, risktakers that thrive in a swing for the fences culture. We are positive, passionate, and a lot of fun to work with. We take no shortcuts and constantly strive to be the best in the world at what we do.</p>
                <p>We’re always looking for fresh talent to join our amazing team of hackers, hipsters, and hustlers. Think you have what it takes to join the Cru? Get in touch – we’re always up for connecting.</p>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid team">
    <div class="container team-wrapper">
        <div class="row team-title">
            <h2>Meet the Executive Team</h2>
        </div>
        <div class="row justify-content-around">
            <div class="col-lg-2 team-member">
            	<figure class="team-member-image"><img src="<?php echo CARECRU_IMG; ?>/About/Headshots/Justin-Sharp-Linkedin-Square-300x300.jpeg" alt="Justin Sharp" class="img-fluid rounded-circle"></figure>
                <h2>Justin Sharp</h2>
                <span>Founder | CTO</span>
                <p>Justin is no stranger to start-ups. He was previously CTO and co-founder of Varafy, an advanced authoring platform for post-secondary learning in STEM fields. He is the driving force behind CareCru’s talented team of engineers who make no compromises on quality and the user experience.</p>
            </div>
            <div class="col-lg-2 team-member">
                <figure class="team-member-image"><img src="<?php echo CARECRU_IMG; ?>/About/Headshots/Donald-Park-Square-300x300.jpg" alt="Donald Park" class="img-fluid rounded-circle"></figure>
                <h2>Donald Park</h2>
                <span>Chief Operating Officer</span>
                <p>Donald is a results-driven operations specialist with over 15 years experience in dentistry including previous senior roles with Straumann, Biomet 3i, and Den-Mat Holdings. He is a sought after advisor and consultant to leading dental groups and key dental opinion leaders across North America.</p>
            </div>
            <div class="col-lg-2 team-member">
                <figure class="team-member-image"><img src="<?php echo CARECRU_IMG; ?>/About/Headshots/Lonny-McLean-LinkedIn-300x300.jpg" alt="LonnyMcLean" class="img-fluid rounded-circle"></figure>
                <h2>Lonny MClean</h2>
                <span>Founder | CEO</span>
                <p>Lonny is an accomplished entrepreneur with multiple successful outcomes as a senior executive. He has more than 20 years’ experience building software companies, 2 of which he founded. His previous venture, Layer 7, was successfully acquired by CA Technologies in 2013.</p>
            </div>
            <div class="col-lg-2 team-member">
                <figure class="team-member-image"><img src="<?php echo CARECRU_IMG; ?>/About/Headshots/Ric-Merrifield-LinkedIn-300x300.png" alt="Ric Merrifield" class="img-fluid rounded-circle"></figure>
                <h2>Ric Merrifield</h2>
                <span>VP of Product and Strategy</span>
                <p>Ric is a proven product and customer experience architect who has held senior roles at Microsoft and Synapse Product Development. He was an early SaaS innovator, creating a CRM solution in 1999. More recently, he led major projects including the Disney MagicBand and Starbucks Mobile Order & Pay.</p>
            </div>
            <div class="col-lg-2 team-member">
                <figure class="team-member-image"><img src="<?php echo CARECRU_IMG; ?>/About/Headshots/Mark-LinkedIn-2-Square-300x300.png" alt="Mark Joseph" class="img-fluid rounded-circle"></figure>
                <h2>Mark Joseph</h2>
                <span>Founder | VP of Sales & Marketing</span>
                <p>Mark is a growth hacking, demand generation expert. He brings a unique set of skills borrowed from his time as a Senior Brand & Marketing executive and co-founder of a digital marketing agency. Mark is passionate about finding unique, creative ways to build demand and tell the CareCru Story.</p>
            </div>
            
        </div>
    </div>
</section>


<?php get_footer(); ?>