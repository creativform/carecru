<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/

// Add separated file for this page
add_action('wp_enqueue_scripts', 'carecru_home_css', 1);
function carecru_home_css(){
	wp_enqueue_style('carecru-home', CARECRU_CSS . '/home.css', array('carecru-bootstrap'), CARECRU_VERSION);
}

get_header();
?>
<div class="clearfix"></div>
<section class="container-fluid" id="Home">
	<div class="container">
        <div class="row align-items-center justify-content-center" id="Donna">
            <div class="col-lg-8 donna-intro">
                <h2>Hi, I’m <strong>Donna</strong></h2>
                <h4>I’m here to help manage and grow your dental practice</h4>
                <a href="#TeamMembers" class="btn btn-danger btn-lg text-white mt-5 desktop scroll-to">Let's get started</a>
            </div>
            <div class="col-lg-4 mobile-button">
            	<a href="#TeamMembers" class="btn btn-danger btn-lg text-white mt-5 mobile scroll-to">Let's get started</a>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section class="container-fluid" id="TeamMembers">
	<div class="container text-center mt-5 mb-5">
        <div class="row align-items-center justify-content-center">
            <h3 class="col-12 mb-5 section-title">3 Team Members in 1 Virtual Assistant</h3>
            <div class="col-lg-4">
            	<div class="card border-0 mt-3 card-link">
                    <div class="card-img-top overly-image">
                    	<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Front_Office_Assistant_Icon_Grey.png" alt="Front Office Assistant">
                        <div class="overly-image-hover">
                        	<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Front_Office_Assistant_Icon_Blue.png" alt="Front Office Assistant">
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">Front Office Assistant</h3>
                        <p class="card-text">Fills and optimizes practitioner schedules while improving the overall patient experience</p>
                        <div class="card-button">
                        	<a href="#" class="btn">Book a Growth Call</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
            	<div class="card border-0 mt-3 card-link">
                    <div class="card-img-top overly-image">
                    	<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Marketing_Specialist_Icon_Grey.png" alt="Front Office Assistant">
                        <div class="overly-image-hover">
                        	<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Marketing_Specialist_Icon_Blue.png" alt="Front Office Assistant">
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">Marketing Specialist</h3>
                        <p class="card-text">Acquires new patients each month and manages your online presence and reputation</p>
                        <div class="card-button">
                        	<a href="#" class="btn">Book a Growth Call</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
            	<div class="card border-0 mt-3 card-link">
                    <div class="card-img-top overly-image">
                    	<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Practice_Consultant_Icon_Grey.png" alt="Front Office Assistant">
                        <div class="overly-image-hover">
                        	<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Practice_Consultant_Icon_Blue.png" alt="Front Office Assistant">
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">Practice Consultant</h3>
                        <p class="card-text">Reveals insights and opportunities for growth - proactively taking action to achieve the best results</p>
                        <div class="card-button">
                        	<a href="#" class="btn">Book a Growth Call</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section class="container-fluid pt-5 pb-5" id="HappyCustomers">
	<div class="container text-center mt-5 mb-5">
        <div class="row align-items-center justify-content-center">
            <h3 class="col-12 mb-5 section-title">
            	A Few Of Our Happy Customers
            </h3>
            <div class="col-12">
            	<div class="row no-gutters align-items-center justify-content-center row-eq-height">
                	<div class="col-md-3 image-container">
                    	<div class="overly-image">
                            <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Smile-Source_Logo-MidDarkGrey.png" alt="Smile Source">
                            <div class="overly-image-hover">
                                <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Smile-Source_Logo.png" alt="Smile Source">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 image-container">
                    	<div class="overly-image">
                            <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Dr.-Ed-Lowe_Logo-MidDarkGrey.png" alt="Dr. Ed Lowe">
                            <div class="overly-image-hover">
                                <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Dr.-Ed-Lowe_Logo.png" alt="Dr. Ed Lowe">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 image-container">
                    	<div class="overly-image">
                            <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Made-Ya-Smile_Logo-MidDarkGrey.png" alt="Made Ya Smile Dental">
                            <div class="overly-image-hover">
                                <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Made-Ya-Smile_Logo.png" alt="Made Ya Smile Dental">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 image-container">
                    	<div class="overly-image">
                            <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Dental-Gallery-Logo-MidDarkGrey.png" alt="Dental Gallery">
                            <div class="overly-image-hover">
                                <img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Dental-Gallery-Logo.png" alt="Dental Gallery">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section class="container-fluid pt-5 pb-5" id="CaseStudies">

	<article class="row align-items-center justify-content-center">
    	<div class="col-md-6 has-hilight mobile">
        	<div class="hilight">
            	<img class="img-fluid rounded-circle" src="<?php echo CARECRU_IMG; ?>/Home/Ed_Lowe.jpg" alt="Ed Lowe">
                <h3 class="section-title mb-3">Dr. Ed Lowe Increased New Patients by 50% Without Any Added Effort</h3>
                <p>Dr. Lowe and his staff wanted to increase the number of new patient appointments each month without increasing their advertising budget.</p>
                <p>The CareCru team designed a brand new website that was mobile responsive, Google-friendly, and optimized to convert. Then Donna went to work—ensuring Dr. Lowe’s practice was attracting new patients and filling their schedule.</p>
                <p>Within weeks, Dr. Lowe’s Office had doubled their 5-star reviews, attracted 50% more new patients, and freed up 100+ hours of staff time.</p>
                <blockquote class="blockquote">
                	<p class="mb-3">"CareCru has increased our new patients by 50%. The best part is, this has all been done automatically. Our team now has more time to focus on higher value work."</p>
                    <footer class="blockquote-footer"><strong>Dr. Ed Lowe</strong>, President<br><cite>The Lowe Centre for Cosmetic & Implant Dentistry</cite></footer>                    
                </blockquote>
            </div>
        </div>
    
    	<div class="col-md-6 has-hilight desktop">
        	<div class="hilight">
            	<img class="img-fluid rounded-circle" src="<?php echo CARECRU_IMG; ?>/Home/Ed_Lowe.jpg" alt="Ed Lowe">
                <blockquote class="blockquote">
                	<p class="mb-3">"CareCru has increased our new patients by 50%. The best part is, this has all been done automatically. Our team now has more time to focus on higher value work."</p>
                    <footer class="blockquote-footer"><strong>Dr. Ed Lowe</strong>, President<br><cite>The Lowe Centre for Cosmetic & Implant Dentistry</cite></footer>                    
                </blockquote>
            </div>
        </div>
        <div class="col-md-6 desktop">
        	<h3 class="section-title mb-3">Dr. Ed Lowe Increased New Patients by 50% Without Any Added Effort</h3>
            <p>Dr. Lowe and his staff wanted to increase the number of new patient appointments each month without increasing their advertising budget.</p>
			<p>The CareCru team designed a brand new website that was mobile responsive, Google-friendly, and optimized to convert. Then Donna went to work—ensuring Dr. Lowe’s practice was attracting new patients and filling their schedule.</p>
            <p>Within weeks, Dr. Lowe’s Office had doubled their 5-star reviews, attracted 50% more new patients, and freed up 100+ hours of staff time.</p>
        </div>
    </article>
    
    <article class="row align-items-center justify-content-center">
    	<div class="col-md-6 text-right desktop">
        	<h3 class="section-title mb-3">Gallery Dental Gets an Additional 10 New Patients in Their First Month</h3>
            <p>Gallery Dental wanted to provide an easy way for patients to schedule appointments outside of office hours, but knew it would be difficult given their complex scheduling processes that they had built over the years.</p>
			<p>The CareCru team took the time to learn their scheduling workflow, and configured Donna to manage the schedule the same way they had done in the past. This included writing specific notes in appointments, splitting schedules between doctors and hygienists, and moving and managing appointments to maximize capacity.</p>
            <p>In the first month alone, Donna scheduled 10 new patients, 7 of which booked outside of office hours.</p>
        </div>
        <div class="col-md-6 has-hilight desktop">
        	<div class="hilight hilight-light">
            	<img class="img-fluid rounded-circle" src="<?php echo CARECRU_IMG; ?>/Home/Gur_Manhas.jpg" alt="Gur Manhas">
                <blockquote class="blockquote">
                	<p class="mb-3">"We’ve seen our new patients increase dramatically. They love how easy it is to book with the online scheduler."</p>
                    <footer class="blockquote-footer"><strong>Gur Manhas</strong>, Office Manager<br><cite>Gallery Dental</cite></footer>                    
                </blockquote>
            </div>
        </div>
        <div class="col-md-6 has-hilight mobile">
        	<div class="hilight hilight-light">
            	<img class="img-fluid rounded-circle" src="<?php echo CARECRU_IMG; ?>/Home/Gur_Manhas.jpg" alt="Gur Manhas">
                <h3 class="section-title mb-3">Gallery Dental Gets an Additional 10 New Patients in Their First Month</h3>
                <p>Gallery Dental wanted to provide an easy way for patients to schedule appointments outside of office hours, but knew it would be difficult given their complex scheduling processes that they had built over the years.</p>
                <p>The CareCru team took the time to learn their scheduling workflow, and configured Donna to manage the schedule the same way they had done in the past. This included writing specific notes in appointments, splitting schedules between doctors and hygienists, and moving and managing appointments to maximize capacity.</p>
                <p>In the first month alone, Donna scheduled 10 new patients, 7 of which booked outside of office hours.</p>
                <blockquote class="blockquote">
                	<p class="mb-3">"We’ve seen our new patients increase dramatically. They love how easy it is to book with the online scheduler."</p>
                    <footer class="blockquote-footer"><strong>Gur Manhas</strong>, Office Manager<br><cite>Gallery Dental</cite></footer>                    
                </blockquote>
            </div>
        </div>
    </article>
    
    <article class="row align-items-center justify-content-center">
    	<div class="col-md-6 has-hilight mobile">
        	<div class="hilight">
            	<img class="img-fluid rounded-circle" src="<?php echo CARECRU_IMG; ?>/Home/Shauna_Lenius.jpg" alt="Shauna Lenius">
                <h3 class="section-title mb-3">Medi-Dent Sees Over 25 New Patients in Their First Month</h3>
                <p>Medi-Dent relied on staff and piecemeal solutions to manage their web presence. This left them wanting more.</p>
                <p>Donna began to quickly convert website visitors into new patients and helped retain those patients and keep them loyal over time.</p>
                <p>Within their first month, they saw over 25 new patients that booked with their practice.</p>
                <blockquote class="blockquote">
                	<p class="mb-3">"Immediately after getting CareCru we started seeing 1 - 2 new patients a day coming online! They get results and are wonderful to work with. I recommend them for any dental group or practice that is looking to grow their business."</p>
                    <footer class="blockquote-footer"><strong>Shauna Lenius</strong>, President<br><cite>Medi-Dent Developments</cite></footer>                    
                </blockquote>
            </div>
        </div>
        
        <div class="col-md-6 has-hilight desktop">
        	<div class="hilight hilight-grey">
            	<img class="img-fluid rounded-circle" src="<?php echo CARECRU_IMG; ?>/Home/Shauna_Lenius.jpg" alt="Shauna Lenius">
                <blockquote class="blockquote">
                	<p class="mb-3">"Immediately after getting CareCru we started seeing 1 - 2 new patients a day coming online! They get results and are wonderful to work with. I recommend them for any dental group or practice that is looking to grow their business."</p>
                    <footer class="blockquote-footer"><strong>Shauna Lenius</strong>, President<br><cite>Medi-Dent Developments</cite></footer>                    
                </blockquote>
            </div>
        </div>
        <div class="col-md-6 desktop">
        	<h3 class="section-title mb-3">Medi-Dent Sees Over 25 New Patients in Their First Month</h3>
            <p>Medi-Dent relied on staff and piecemeal solutions to manage their web presence. This left them wanting more.</p>
            <p>Donna began to quickly convert website visitors into new patients and helped retain those patients and keep them loyal over time.</p>
            <p>Within their first month, they saw over 25 new patients that booked with their practice.</p>
        </div>
    </article>

	<article class="container text-center" id="Benefits">
        <div class="row row-eq-height">
            <h3 class="col-12 mb-5 section-title">
            	Benefits
            </h3>
            <div class="col-md-4 mt-3">
                <div class="benefit-icon">
                    <img class="img-fluid img-icon" src="<?php echo CARECRU_IMG; ?>/Home/Increase-Production.png" alt="Increase Production">
                    <h4>Increase Production by over 20%</h4>
                </div>
			</div>
            <div class="col-md-4 mt-3">
                <div class="benefit-icon">
                    <img class="img-fluid img-icon" src="<?php echo CARECRU_IMG; ?>/Home/All-in-One-Patient.png" alt="All-in-One Patient">
                    <h4>All-in-one Patient Relationship Management</h4>
                </div>
			</div>
            <div class="col-md-4 mt-3">
            	<div class="benefit-icon">
                    <img class="img-fluid img-icon" src="<?php echo CARECRU_IMG; ?>/Home/Attract-Up-to-50.png" alt="Attract Up to 50">
                    <h4>Attract up to 50% More New Patients</h4>
                </div>
			</div>
            <div class="col-md-4 mt-3">
                <div class="benefit-icon">
                    <img class="img-fluid img-icon" src="<?php echo CARECRU_IMG; ?>/Home/Drive-Patient-loyalty.png" alt="Drive Patient Loyalty">
                    <h4>Drive Patient Loyalty and Engagement</h4>
                </div>
			</div>
            <div class="col-md-4 mt-3">
                <div class="benefit-icon">
                    <img class="img-fluid img-icon" src="<?php echo CARECRU_IMG; ?>/Home/Give-Donna-Over-40.png" alt="Give Donna Over 40">
                    <h4>Give Donna over 40 hours of weekly tasks</h4>
                </div>
			</div>
            <div class="col-md-4 mt-3">
                <div class="benefit-icon">
                    <img class="img-fluid img-icon" src="<?php echo CARECRU_IMG; ?>/Home/Gain-Actionable-Insights.png" alt="Gain Actionable Insights">
                    <h4>Gain Actionable Insights on Your Business</h4>
                </div>
			</div>
        </div>
    </article>
    
</section>

<div class="clearfix"></div>

<section class="container-fluid" id="Capabilities">
	<div class="container text-center mt-5 mb-5">
        <div class="row align-items-center justify-content-center">
            <h3 class="col-12 mb-5 section-title">
            	A Few of Donna’s Capabilities
            </h3>
            <div class="col-md-6 order-1">
				<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Turbo_Charge_your_Practice.png" alt="Turbo Charge your Practice">
            </div>
            <div class="col-md-6 text-left order-2">
				<h3 class="section-title">Turbo Charge Your Practice With Artificial Intelligence</h3>
                <p>Donna is designed to use the latest online marketing tactics to attract, acquire, and retain patients. Rely on Donna to fill your schedule, so you can focus on patient care.</p>
            </div>
            <div class="col-md-6 order-4">
				<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Boost_Patient_Engagement.png" alt="Boost Patient Engagement">
            </div>
            <div class="col-md-6 text-right order-3">
				<h3 class="section-title">Boost Patient Engagement and Loyalty</h3>
                <p>Provide an exceptional end-to-end patient experience. Keep them engaged all year round with the right message at the right time.</p>
                <blockquote class="blockquote mt-5">
                	<p class="mb-3">"We turned on CareCru and the next morning had 5 returning patient appointment requests!"</p>
                    <footer class="blockquote-footer"><cite>Maple Place Dental</cite></footer>                    
                </blockquote>
            </div>
            <div class="col-md-6 order-5">
				<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Powerful_Practice_Intelligence.png" alt="Powerful Practice Intelligence">
            </div>
            <div class="col-md-6 text-left order-6">
				<h3 class="section-title">Powerful Practice Intelligence</h3>
                <p>CareCru’s intelligence reports give you actionable insight in ways you had not previously been able to see. Measure and compare your dental offices using key performance indicators and industry benchmarks.</p>
            </div>
            <div class="col-md-6 order-8">
				<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/Seamlessly_Integrate.png" alt="Seamlessly Integrate">
            </div>
            <div class="col-md-6 text-right order-7">
				<h3 class="section-title">Seamlessly Integrate With Your Practice Software</h3>
                <p>Read, write and track integration capabilities with multiple practice management software in real-time. The CareCru platform is both fully secure and fully compliant.</p>
                <blockquote class="blockquote mt-5">
                	<p class="mb-3">"With CareCru, everything I need to know is right there, at my fingertips."</p>                    
                </blockquote>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section class="container-fluid" id="OurOffer">
	<div class="container mt-5 mb-5">
    	<div class="row align-items-center pl-5 pr-5" id="OurOfferList">
            <div class="col-lg-2 desktop">
           		<img class="img-fluid" src="<?php echo CARECRU_IMG; ?>/Home/SuperDonna.png" alt="Super Donna">
            </div>
           	<div class="col-md-12 col-lg-10">
           		<div class="row align-items-center pl-5 pr-5">
                    <h3 class="col-12 section-title text-left pl-0 pr-0">
                        Our Offer—Here’s what you get!
                    </h3>
                    <p class="text-left mb-5 subtitle">For one low price, Donna provides the following</p>
                </div>
                <div class="row text-left mb-3">
                    <div class="col-md-6">
                        <ul class="list-unstyled list-ok">
                            <li>Guaranteed New Patients each Month</li>
                            <li>Guaranteed Production Growth</li>
                            <li>No Onboarding Set up Fee</li>
                            <li>Dedicated Customer Success Manager</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-unstyled list-ok">
                            <li>60 Days Money back guarantee</li>
                            <li>No Cancellation Charges</li>
                            <li>No Contract</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col col-banner">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h3>Book a No Strings Attached Demo</h3>
                        <h4>It only takes 30 minutes</h4>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="#" class="btn btn-lg">GET A PRICE</a>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>

<div class="clearfix"></div>
<?php
$accordion = new WP_Query( array(
	'post_type' => 'faq',
	'posts_per_page' => -1,
	'tax_query' => array(
		'taxonomy' => 'faq-category',
		'field' => 'slug',
		'terms' => 'landing-page'
	)
) );
if($accordion->have_posts()) :
?>
<section class="container-fluid" id="FAQ">
	<div class="container mt-5 mb-5">
    	<div class="row align-items-center justify-content-center">
            <h3 class="col-12 mb-5 section-title text-center">
            	Frequently Asked Questions
            </h3>
            <div class="col-12">
            	<div class="accordion" id="AccordionFAQ">
				<?php
                    $i=-999999;
                    while ( $accordion->have_posts() ) : $accordion->the_post();
                ?>
                
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link<?php echo $i===0 ? '' : ' collapsed'; ?>" type="button" data-toggle="collapse" data-target="#FAQ-<?php the_ID(); ?>" aria-expanded="<?php echo $i===0 ? 'true' : 'false'; ?>" aria-controls="FAQ-<?php the_ID(); ?>">
                                    <?php the_title(); ?>
                                </button>
                            </h5>
                        </div>
                        
                        <div id="FAQ-<?php the_ID(); ?>" class="collapse<?php echo $i===0 ? ' show' : ''; ?>" aria-labelledby="FAQ-<?php the_ID(); ?>" data-parent="#AccordionFAQ">
                            <div class="card-body">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <?php ++$i; endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php get_footer();