 <?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * Template Name: Press Kit Page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/
// Add separated file for this page

add_action('wp_enqueue_scripts', 'carecru_presskit_css', 1);

function carecru_presskit_css(){

	wp_enqueue_style('carecru-home', CARECRU_CSS . '/press_kit.css', array('carecru-bootstrap'), CARECRU_VERSION);

}

get_header();

?>

<section class="conteiner-fluid press_kit_header">
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 press_kit_header__container">
                <h1>Press Kit</h1>
            </div>
        </div>
    </div>
    
</section>

<section class="container">
    <div class="row brand-personality">
        <div class="col-lg-4">
            <h2>Brand Personality</h2>
        </div>
        <div class="col-lg-8">
            <p>These keywords describe the CareCru brand, the mood it represents, and the emotions the audience feels when interacting with the brand</p>
            <span>Honesty, Integrity, Humility, Ambition, Humor, Current, Clean, Playful, Modern, Contemporary, Versatile, Savvy, Simple, Intelligent, Catalyst, Personalized, Proactive, Forward-looking, Credible, Brilliant</span>
        </div>
    </div>
</section>

<section class="container brand-resources">
    
    <div class="row no-margin brand-resources-header">
        
        <div class="col-lg-12">
            <h2>Band Resources</h2>
        </div>
        
    </div>
    
    
    
    <div class="row no-margin brand-resources-logo">
        
        <div class="brand-wrapper col-lg-1 col-md-12 d-flex justify-content-between">
            
            <p class="brand-number">01</p>
            
            <figure class="v-line"></figure>
            
        </div>
        
        <div class="col-lg-11 col-md-12">
            
            <div class="row no-margin brand-logo-title">
                <div class="col-lg-12">
                    <h2>Logo</h2>
                </div>
            </div>
            
            <div class="row no-margin brand-logo-items">
                <figure class="col-lg-3 col-md-6">
                    <a href="<?php echo CARECRU_IMG; ?>/Press Kit/logo/carecru-logo-1.png" target="_blank"><img src="<?php echo CARECRU_IMG; ?>/Press Kit/carecru-logo-1.png"></a>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <a href="<?php echo CARECRU_IMG; ?>/Press Kit/logo/carecru-logo-2.png" target="_blank"><img src="<?php echo CARECRU_IMG; ?>/Press Kit/carecru-logo-2.png"></a>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <a href="<?php echo CARECRU_IMG; ?>/Press Kit/logo/carecru-logo-3.png" target="_blank"><img src="<?php echo CARECRU_IMG; ?>/Press Kit/carecru-logo-3.png"></a>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <a href="<?php echo CARECRU_IMG; ?>/Press Kit/logo/carecru-logo-4.png" target="_blank"><img src="<?php echo CARECRU_IMG; ?>/Press Kit/carecru-logo-4.png"></a>
                </figure>
            </div>
        </div>
        
    </div>
    
    <div class="row no-margin brand-resources-palette">
        
        <div class="brand-wrapper col-lg-1 d-flex justify-content-between">
            
            <p class="brand-number">02</p>
            
            <figure class="v-line"></figure>
            
        </div>
        
        <div class="col-lg-11">
            
            <div class="row no-margin brand-palette-title">
                <div class="col-lg-12">
                    <h2>Colour Palette</h2>
                </div>
            </div>
            
            <div class="row no-margin align-items-center justify-content-center brand-palette-items text-center">
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 pallete navy-blue">
                    <h6 class="desktop">Navy Blue</h6>
                    <code>#206477</code>
                    <h6 class="mobile">Navy Blue</h6>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 pallete dark-blue">
                    <h6 class="desktop">Dark Blue</h6>
                    <code>#184B5A</code>
                    <h6 class="mobile">Dark Blue</h6>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 pallete extremely-dark-blue">
                    <h6 class="desktop">Extremely Dark Blue</h6>
                    <code>#2F3844</code>
                    <h6 class="mobile">Extremely Dark Blue</h6>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 pallete teal-green">
                    <h6 class="desktop">Teal Green</h6>
                    <code>#2CC4A7</code>
                    <h6 class="mobile">Teal Green</h6>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 pallete pastel-yellow">
                    <h6 class="desktop">Pastel Yellow</h6>
                    <code>#FFC45A</code>
                    <h6 class="mobile">Pastel Yellow</h6>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 pallete salmon-red">
                    <h6 class="desktop">Salmon Red</h6>
                    <code>#FF715A</code>
                    <h6 class="mobile">Salmon Red</h6>
                </div>
            </div>
            
            <div class="row no-margin align-items-center justify-content-center brand-palette-items text-center">
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 pallete dark-grey">
                    <h6 class="desktop">Dark Grey</h6>
                    <code>#6E6E6E</code>
                    <h6 class="mobile">Dark Grey</h6>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 pallete grey">
                    <h6 class="desktop">Grey</h6>
                    <code>#CDCDCD</code>
                    <h6 class="mobile">Grey</h6>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-12 pallete light-grey">
                    <h6 class="desktop">Light Grey</h6>
                    <code>#EFEFEF</code>
                    <h6 class="mobile">Light Grey</h6>
                </div>
            </div>
            
        </div>
        
    </div>
        
    <div class="row no-margin brand-resources-fonts">
        
        <div class="brand-wrapper col-lg-1 d-flex justify-content-between">
            
            <p class="brand-number">03</p>
            
            <figure class="v-line"></figure>
            
        </div>
        
        <div class="col-lg-11">
            
            <div class="row no-margin brand-fonts-title">
                <div class="col-lg-12">
                    <h2>Fonts</h2>
                    <p>The following fonts are to be used for all CareCru properties, material and assets when both, possible and allowed.</p>
                </div>
            </div>
            
            <div class="row no-margin brand-fonts">
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto light">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Light</p>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto light-italic">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Light Italic</p>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto regular">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Regular</p>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto italic">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Italic</p>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto medium">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Medium</p>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto medium-italic">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Medium Italic</p>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto bold">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Bold</p>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 fonts-item roboto bold-italic">
                    <p class="font-name">Roboto</p>
                    <p class="font-weight">Bold Italic</p>
                </div>
                
            </div>
            
        </div>
        
    </div>
        
    <div class="row no-margin brand-resources-products">
        
        <div class="brand-wrapper col-lg-1 d-flex justify-content-between">

            <p class="brand-number">04</p>

            <figure class="v-line"></figure>

        </div>

        <div class="col-lg-11">

            <div class="row no-margin brand-products-title">

                <div class="col-lg-12">
                    <h2>Product Images</h2>
                    <p>The following images are to be used for all CareCru properties, material and assets when both, possible and allowed.</p>
                </div>

            </div>

            <div class="row no-margin brand-products-items">
                
                <div class="col-lg-6">
                    <figure>
                        <img src="<?php echo CARECRU_IMG; ?>/Press Kit/tablet.png">
                        <button type="button">Print</button>
                    </figure>
                </div>
                
                <div class="col-lg-6">
                    <figure>
                        <img src="<?php echo CARECRU_IMG; ?>/Press Kit/tablet.png">
                        <button type="button">Web</button>
                    </figure>
                </div>
                
            </div> 
            
        </div>
            
        </div>    
            
</section>

<?php get_footer(); ?>
