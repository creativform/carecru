<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * Template Name: Contact Page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/

add_action('wp_enqueue_scripts', 'carecru_contact_css', 1);
function carecru_contact_css() {
    wp_enqueue_style('carecru-home', CARECRU_CSS . '/contact.css', array('carecru-general'), CARECRU_VERSION);
}

add_filter( 'body_class', function( $classes ) {
    return array_merge( $classes, array( 'contact-page' ) );
} );

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();
if($elementor_page = get_post_meta( get_the_ID(), '_elementor_edit_mode', true )) :
	the_content();
else :
?>

<section class="container-fluid contact-page-header">
    <div class="row">
        <h2 class="col">Contact Us</h2>
    </div>
</section>

<section class="container-fluid">
    <div class="container contact-form-wrapper"> 
        <div class="row">
            <div class="col-lg-7 col-md-12 contact-form">
                <h2 class="form-title">Get In Touch</h2>
                <?php the_content(); ?>
            </div>
            
            <div class="col-lg-5 col-md-12 contact-info-wrapper">
                
                    <div class="row contact-info">
                        <div class="col-lg-6">
                            <p>Our office</p>
                            <h2>543 Granville St, 8th Floor<br class="mobile"> Vancouver, BC <br class="mobile">V6C 1X8</h2>
                        </div>
                    </div>

                    <div class="row contact-info justify-content-between">
                        <div class="col-lg-6">
                            <p>Call Us</p>
                            <h2>1 888 870 0077</h2>
                        </div>
                        <div class="col-lg-6">
                            <p>Send Us An Email</p>
                            <h2>hello@carecru.com</h2>
                        </div>
                    </div>
                <div class="container">
                    <div class="row contact-map desktop">
                        <div class="col-12">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2602.5790963303148!2d-123.11808538390892!3d49.28437227933139!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54867178af768921%3A0xa411ccdd6e261dc0!2s6%2C+543+Granville+St%2C+Vancouver%2C+BC+V6C+1X8%2C+Canada!5e0!3m2!1sen!2sfi!4v1534447044947" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 contact-subscribe">
                            <h2>Get Latest Updates</h2>
                            <h3>Keep up to date with CareCru</h3>

                            <form role="form">
                                <div class="form-group d-flex">
                                    <input type="email" placeholder="Email">
                                    <button type="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</section>
<?php endif; endwhile; else: ?>
<p><?php _e("Sorry, can't display page content for unexpected reason.", 'carecru'); ?></p>
<?php endif; ?>
<?php get_footer();