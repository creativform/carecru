<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * 404 Page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/

get_header();
?>
<div class="container nt-3 mb-5" id="P404">
    <div class="row align-items-center">
        <div class="col-lg-4 text-center order-2">
            <h1 class="h1"><?php _e('Oops!', 'carecru'); ?></h1>
            <h3><?php _e("We can't seem to find the page you looking for.", 'carecru'); ?></h3>
            <h4><?php printf(__('Error Code: %d', 'carecru'), 404); ?></h4>
            <p><?php _e("The page you're trying to access doesn't appear to exists.", 'carecru'); ?></p>
            <p><?php printf(__('Looking for anything specific? Maybe our %s can help? Or you can try search:', 'carecru'), '<a href="' . home_url() . '">' . __('Home Page', 'carecru') . '</a>'); ?></p>
            <?php get_template_part( 'includes/search', 'form' ); ?>
        </div>
        <div class="col-lg-6 order-1">
            <figure><img src="<?php echo CARECRU_IMG; ?>/Donna-on-Laptop.png" alt="Donna on Laptop" class="img-fluid"></figure>
        </div>
    </div>
</div>
<?php get_footer();