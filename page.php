<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**
 * The page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 */
get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post();
if($elementor_page = get_post_meta( get_the_ID(), '_elementor_edit_mode', true )) :
	the_content();
else : ?>
<section class="container-fluid" id="SinglePage">
	<article class="container">
    	<div class="row no-gutters align-items-end">
            <h1 class="col-sm-8"><?php the_title(); ?></h1>
            <div class="col-sm-4 modified-date">
            	<?php the_modified_date( get_option('date_format'), __('Last Edited on ','carecru'), '', true ); ?> 
            </div>
        </div>
        <hr>
		<?php the_content(); ?>
    </article>
</section>
<?php endif; endwhile; else: ?>
<p><?php _e("Sorry, can't display page content for unexpected reason.", 'carecru'); ?></p>
<?php endif; ?>
<?php get_footer();