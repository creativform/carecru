/**********************************
 * The functions of theme
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/
(function($){

var CARECRU = {
	
	/*
	 * Setup header and main menu
	*/
	header : function(){
		var CC = {
			window : {
				width : $(window).width(),
				height : $(window).height()
			},
			logo : []
		}
		
		$('.logo > a > img', '#MainMenu').each(function(){
			var $this = $(this);
			CC.logo.push({
				original : $this.attr('data-src'),
				sticky : $this.attr('data-sticky'),
				object : $this
			});
		});
		
		
		var calculate = function(){
			var scroll = $(window).scrollTop();
			if (scroll >= ( ($(window).width() > 992 ? 0.3 : 0.1) * CC.window.height)) {
				$("#MainMenu").addClass('fixed');
				
				for(var i = 0; i<CC.logo.length; i++)
				{
					if(CC.logo[i].sticky){
						CC.logo[i].object.attr('src', CC.logo[i].sticky);
					}
				}
				
			} else {
				$("#MainMenu").removeClass('fixed');
				
				for(var i = 0; i<CC.logo.length; i++)
				{
					if(CC.logo[i].sticky){
						CC.logo[i].object.attr('src', CC.logo[i].original);
					}
				}
			}
		}
		
		// Scroll setup
		calculate();
		$(window).scroll(calculate);
	},
	
	/*
	 * MOBILE NAVIGATION
	*/
	mobile_nav : function(){
		$('.navbar-toggler').on('click touchstart', function(e){
			e.preventDefault();
			
			var $this = $(this),
				open = $this.attr('aria-open');

			if(open == 'true')
			{
				$('#MobileMenu').fadeOut(100);
				$this.attr('aria-open', 'false');
				$('body').removeClass('has-popup');
			}
			else
			{
				$('#MobileMenu').fadeIn(500,function(){
					$(this).focus();
				});
				$this.attr('aria-open', 'true');
				$('body').addClass('has-popup');
			}
			
		});
		
		$('#MobileMenuClose').on('click touchstart', function(e){
			e.preventDefault();
			$('#MobileMenu').fadeOut(100);
			$('.navbar-toggler').attr('aria-open', 'false');
			$('body').removeClass('has-popup');
		});
	},
	
	/*
	 * POPUP
	*/
	popup : function(){
		$('.get-a-price').on('click touchstart', function(e){
			var $this = $(this),
				open = $this.attr('aria-open');

			if(open == 'true')
			{
				$('#PopupGetPrice').fadeOut(100);
				$this.attr('aria-open', 'false');
				$('body').removeClass('has-popup');
			}
			else
			{
				$('#PopupGetPrice').fadeIn(500,function(){
					$(this).css({
						height : $(window).height(),
						maxHeight : $(window).height()
					}).focus();
				});
				$this.attr('aria-open', 'true');
				$('body').addClass('has-popup');
			}
		})
		
		$('.PopupGetPriceClose').on('click touchstart', function(e){
			e.preventDefault();
			$('#PopupGetPrice').fadeOut(100);
			$('.get-a-price').attr('aria-open', 'false');
			$('body').removeClass('has-popup');
		});
		
		$(window).resize(function(){
			$('#PopupGetPrice:visible').css({
				height : $(window).height(),
				maxHeight : $(window).height()
			});
		});

		var wpcf7Elm = document.querySelector( '.wpcf7' );
 
		wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
			$('.popup-step-1').fadeOut(100,function(){
				$('.popup-step-2').fadeIn(200);
				$(this).remove();
			});
		}, false );
	},

	/*
	* Slide to
	*/
    scrollTo : function(){  
		$('.scroll-to').on('click touchstart', function(e){
			  var $anchor = $(this).attr("href");
			  var $hrefStart = $anchor.substr(0, 1);
			  if ( $hrefStart == "#" ) {
				  $('html,body').animate({
					  scrollTop: $($anchor).offset().top - ($('#MainMenu').height()*2+32)
				  }, 1500, 'easeInOutExpo');
				  e.preventDefault();
			  } else {
				  window.location.href = $anchor;
			  }
		});
	}

}

$(document).ready(function(){
	CARECRU.header();
	CARECRU.mobile_nav();
	CARECRU.popup();
	CARECRU.scrollTo();
});

}(jQuery || window.jQuery));