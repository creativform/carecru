<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * Search Page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/

global $wp_query;

// We need pagination here
include CARECRU_INC . '/Pagination.php';

$queried_object = get_queried_object();
$current_category = isset($queried_object->term_id) ? $queried_object->term_id : 0;
$current_parent = 0;

$category_array = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false
) );

$category = array();
foreach($category_array as $key => $obj){
    $category[$obj->parent][] = $obj;

    if($obj->term_id === $current_category) $current_parent = $obj->parent;
}

get_header();
?>
<section class="container-fluid" id="SearchPage">
	<div class="container mt-2 mb-5">
    	<aside class="row">
            <aside class="col-lg-3">
                <?php get_template_part( 'includes/search', 'form' ); ?>
                <?php get_template_part( 'includes/menu', 'category' ); ?>
                <div class="desktop"><?php carecru_pagination('',2,$wp_query); ?></div>
            </aside>
            <div class="col-lg-9">

                <div class="row">
                <?php
                    $i = 1; if(have_posts()) :
					while ( have_posts() ) : the_post(); $article_id = get_the_ID(); 
                        get_template_part( 'includes/loop', 'posts' ); 
                    ++$i; endwhile; else: ?>
                    <h2 class="col-12 text-center"><?php _e('No search results. Please try some of the links above.','carecru'); ?></h2>
                <?php endif; ?>
                </div>
                <div class="mobile"><?php carecru_pagination('',2,$wp_query); ?></div>
            </div>
    	</div>
    </div>
</section>
<?php get_footer();