<?php
/**
 * default search form
 */
?>
<div id="SearchForm" class="mt-3 mb-3">
    <form role="search" method="get" class="form search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div class="input-group">
            <input name="s" type="text" class="form-control" placeholder="<?php _e('Search', 'carecru'); ?>" value="<?php echo esc_attr( get_search_query() ); ?>">
            <span class="input-group-btn">
                <button type="submit" value="<?php _e('Search', 'carecru'); ?>" class="btn btn-primary text-white" type="button"><img src="<?php Carecru_Theme::icon('search'); ?>" alt="<?php _e('Search', 'carecru'); ?>" style="max-width: 18px;"></button>
            </span>
        </div>
    </form>
</div>