<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**
 * The index of theme
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 */
global $wp_query;

$page=get_post_type();

// Let's get all we need
$queried_object = get_queried_object();
$current_category = isset($queried_object->term_id) ? $queried_object->term_id : 0;
$current_parent = 0;

$category_array = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false
) );

$category = array();
foreach($category_array as $key => $obj){
    $category[$obj->parent][] = $obj;

    if($obj->term_id === $current_category) $current_parent = $obj->parent;
}

if($page=='faq')
    get_template_part( 'includes/page', 'faq' );
else
    get_template_part( 'includes/page', 'posts' );