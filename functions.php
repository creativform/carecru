<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * The functions of theme
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
 * Global Template Defines
*/
define("CARECRU_VERSION", "0.0.1");

define("CARECRU_URL", rtrim(get_template_directory_uri(), '/'));
define("CARECRU_CSS", CARECRU_URL . '/assets/css');
define("CARECRU_JS", CARECRU_URL . '/assets/js');
define("CARECRU_IMG", CARECRU_URL . '/assets/images');

define("CARECRU_DIR", rtrim(get_template_directory(), '/'));
define("CARECRU_INC", CARECRU_DIR . '/includes');
define("CARECRU_CLASS", CARECRU_INC . '/class');
/*
 * Define and setup blobal class
*/
if(!class_exists('Carecru_Theme')) :
class Carecru_Theme
{
	/* 
	 * Run all scripts
	*/
	function __construct()
	{
		/*
		 * Includes
		*/
		$this->includes();
		/*
		 * Actions
		*/
		$this->add_action('after_setup_theme', 'global_theme_setup');
		$this->add_action('wp_enqueue_scripts', 'enqueue_scripts');
		$this->add_action('widgets_init', 'widgets');
		$this->add_action( 'init', 'custom_posts', 0 );
		$this->add_action( 'customize_register', 'register_logos' );
		/*
		 * Filters
		*/
		$this->add_filter( 'wp_nav_menu_items', 'new_nav_menu_items', 10, 2);
	}
	
	/*
	 * Register all logo types
	*/
	function register_logos( $wp_customize ) {
		
		$wp_customize->add_setting('sticky_logo');
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'sticky_logo', array(
			'label' => __('Sticky Logo', 'carecru'),
			'section' => 'title_tagline',
			'settings' => 'sticky_logo',
			'priority' => 8
		)));
		
		$wp_customize->add_setting('white_logo');
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'white_logo', array(
			'label' => __('White Logo', 'carecru'),
			'section' => 'title_tagline',
			'settings' => 'white_logo',
			'priority' => 8
		)));
		
		
		$wp_customize->add_setting('footer_logo');
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_logo', array(
			'label' => __('Footer Logo', 'carecru'),
			'section' => 'title_tagline',
			'settings' => 'footer_logo',
			'priority' => 8
		)));
	
	}
	
	/*
	 * Includes
	*/
	function includes(){
		include_once CARECRU_CLASS . '/metabox.php';
	}
	
	/* 
	 * Global theme setup
	*/
	function global_theme_setup()
	{
		global $cap, $content_width;
		/** 
		 * This theme styles the visual editor with editor-style.css to match the theme style.
		 */
		add_editor_style();
		/**
		 * Add default posts and comments RSS feed links to head
		 */
		add_theme_support('automatic-feed-links');
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support('post-thumbnails');
		/**
		 * Enable support for search form
		 **/
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		/**
		 * Enable support for Post Formats
		 */
		add_theme_support('post-formats', array(
			'standard',
			'link'
		));
		/**
		 * Make theme available for translation
		 */
		$this->add_filter( 'locale', 'set_locale' );
		load_theme_textdomain('carecru', CARECRU_DIR . '/languages');
		/**
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus(array(
			'primary' => __('Main menu', 'carecru'),
			'mobile-menu' => __('Mobile menu', 'carecru'),
		));
		
		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
		
		// Add theme support for Custom Logo.
		add_theme_support( 'custom-logo', array(
			'width'       => 250,
			'height'      => 40,
			'flex-width'  => true,
		) );
		
		// Only search posts
		$this->add_filter( 'pre_get_posts', 'posts_search');
	}

	/* 
	 * Set to search only posts
	*/
	function posts_search( $query )
	{
		if ($query->is_search) {
			$query->set('post_type', 'post');
		}
		return $query;
	}
	
	/* 
	 * Set locale for the translation
	*/
	function set_locale( $locale ) {
		if ( isset( $_REQUEST['l'] ) ) {
			return sanitize_key( $_REQUEST['l'] );
		}
		return $locale;
	}
	
	/* 
	 * Enqueue style to wordpress
	*/
	function enqueue_scripts( $locale ) {
		// @url https://fonts.google.com/?selection.family=Montserrat:400,900|Roboto:100,400,700,900&query=roboto
		wp_enqueue_style('carecru-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700', 1, CARECRU_VERSION);
		wp_enqueue_style('carecru-fontawesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array('carecru-fonts'), CARECRU_VERSION);
		wp_enqueue_style('carecru-bootstrap', CARECRU_CSS . '/bootstrap.css', 1, CARECRU_VERSION);
		wp_enqueue_style('carecru-general', CARECRU_CSS . '/style.css', array('carecru-bootstrap'), CARECRU_VERSION);
		wp_enqueue_style('carecru', CARECRU_URL . '/style.css', array('carecru-general'), CARECRU_VERSION);
		
		wp_enqueue_script('carecru-bootstrap', CARECRU_JS . '/bootstrap.min.js', array('jquery'), CARECRU_VERSION);
		wp_enqueue_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', array('jquery'), CARECRU_VERSION);
		wp_enqueue_script('carecru', CARECRU_JS . '/carecru.js', array('jquery','carecru-bootstrap'), CARECRU_VERSION);
		wp_localize_script('carecru', 'ajaxurl', self_admin_url( 'admin-ajax.php' ) );
	}
	
	/* 
	 * Register custom posts
	*/
	function custom_posts(){
     
	 	/* 
		 * FAQ
		*/
		register_post_type( 'faq', array(
			'label'               => __( 'F.A.Q.', 'carecru' ),
			'description'         => __( 'Frequently Asked Questions', 'carecru' ),
			'labels'              => array(
				'name'                => __( 'Frequently Asked Questions', 'carecru' ),
				'singular_name'       => __( 'Frequently Asked Question', 'carecru' ),
				'menu_name'           => __( 'F.A.Q.', 'carecru' ),
				'parent_item_colon'   => __( 'Parent F.A.Q.', 'carecru' ),
				'all_items'           => __( 'FAQs', 'carecru' ),
				'view_item'           => __( 'View F.A.Q.', 'carecru' ),
				'add_new_item'        => __( 'Add New F.A.Q.', 'carecru' ),
				'add_new'             => __( 'Add New', 'carecru' ),
				'edit_item'           => __( 'Edit F.A.Q.', 'carecru' ),
				'update_item'         => __( 'Update F.A.Q.', 'carecru' ),
				'search_items'        => __( 'Search F.A.Q.', 'carecru' ),
				'not_found'           => __( 'Not Found', 'carecru' ),
				'not_found_in_trash'  => __( 'Not found in Trash', 'carecru' ),
			),
			'supports'            => array( 'title', 'editor', 'custom-fields', ),
			'taxonomies'          => array( 'faq-category' ), 
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-sos',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		));
		 
		register_taxonomy( 'faq-category', 'faq', array(
			'label'        => __( 'Groups', 'carecru' ),
			'labels'              => array(
				'name'                => _x( 'Frequently Asked Question Groups', 'Post Type General Name', 'carecru' ),
				'singular_name'       => _x( 'Frequently Asked Question Group', 'Post Type Singular Name', 'carecru' ),
				'menu_name'           => __( 'Groups', 'carecru' ),
				'parent_item_colon'   => __( 'Parent Group', 'carecru' ),
				'all_items'           => __( 'All Groups', 'carecru' ),
				'view_item'           => __( 'View Group', 'carecru' ),
				'add_new_item'        => __( 'Add New F.A.Q. Group', 'carecru' ),
				'add_new'             => __( 'Add New', 'carecru' ),
				'edit_item'           => __( 'Edit Group', 'carecru' ),
				'update_item'         => __( 'Update Group', 'carecru' ),
				'search_items'        => __( 'Search Groups', 'carecru' ),
				'not_found'           => __( 'Not Found', 'carecru' ),
				'not_found_in_trash'  => __( 'Not found in Trash', 'carecru' ),
			),
			'public'       => true,
			'rewrite'      => true,
			'hierarchical' => true
		) );
	}
	
	/* 
	 * Register widgets
	*/
	function widgets()
	{
		register_sidebar(array(
			'name' => __('Blog Sidebar', 'carecru') ,
			'id' => 'blog-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="title-sidebar">',
			'after_title' => '</h4>',
		));
		register_sidebar(array(
			'name' => __('Footer Column 1', 'carecru') ,
			'id' => 'footer-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="title-footer">',
			'after_title' => '</h4>',
		));
		register_sidebar(array(
			'name' => __('Footer Column 2', 'carecru') ,
			'id' => 'footer-2',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="title-footer">',
			'after_title' => '</h4>',
		));
		register_sidebar(array(
			'name' => __('Footer Column 3', 'carecru') ,
			'id' => 'footer-3',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="title-footer">',
			'after_title' => '</h4>',
		));
		register_sidebar(array(
			'name' => __('Footer Column 4', 'carecru') ,
			'id' => 'footer-4',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="title-footer">',
			'after_title' => '</h4>',
		));
		register_sidebar(array(
			'name' => __('Footer Column 5', 'carecru') ,
			'id' => 'footer-5',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="title-footer">',
			'after_title' => '</h4>',
		));
	}
	
	/* 
	 * Add Custom items to menu
	*/
	function new_nav_menu_items($items, $arg) 
    {
		if(in_array($arg->theme_location, array('primary','mobile-menu')) !== false)
		{
        	$homelink = '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-button"><a href="javascript:void(0);" id="get-a-price" class="btn btn-danger btn-lg text-white get-a-price" aria-open="false">' . __('GET A PRICE') . '</a></li>';
        	return $items. $homelink;
		}
		else return $items;
    }
	
/* ===================================== */

	public static function icon($name='')
	{
		$icon = array(
			'search' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAgCAYAAADqgqNBAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAftJREFUeNrEl0FrU0EUhU8MCIFCIBIoCP0B3RYKWQUiiuCq0lVWde/Cv+Cq2/6DLroRChVXQtwUuggJDQGhEhAshS4KYkEKCYLh62Yij8u8vJlpYi4MyRvm3nPm3vvOzCsBWpU90gqtCLwhaV9ST9JYEu635+YbD0IHfGMT6BBmHbdescM3uQvcEWd3zi8KvGQabkfSiSdBl5K+uP9lSc8lbXjWvZb0MSXtm8DY7GgINHKYt4ALs34cU4Lsg63xMfC4IEAF+OzpgSjwpglwDVQDg9SBX8a/GeI7e9XaphpvJP0OrNxPSe/MXDum5qMM668Jr03Z7H4Us/Ns5w4S5GIqqZt53ghVuDVJlczcbaJeZf0qLm4h+MQxn1k9EbxmMjEJAZ86EZlZMwG4bHT+0mxo7sHSN/VqRYLvmJ33Y7q95VG2SmCnV4Hvxr8Vq3BnJsAnYC0A+NT4naXI6zbwxwQaAa9ynHeBH54T7gJYTznV9iQd5qhYV9KNpKeuuWpzqvlN0jO3Puoy8Rb4y8OtMAOac1wOA0FOPQdLEIGiurwEDoDzDMCVa6r3rk8EbKUQUMrdK2dEE1gkeBGBkSWwaPAoAssADyawLPAiAj2fyCzatiR1cgTpybK/1QaSXnguKEeSbpeZdluCa5fyD7Mreek/fiJXXfr/XVzuBwCfhGoHNeKLCgAAAABJRU5ErkJggg=='
		);
		
		if(isset($icon[$name])) echo $icon[$name];
	}

/* ===================================== */
	
	/* 
	 * Hook for add_action()
	*/
	protected function add_action($tag, $function_to_add, $priority = 10, $accepted_args = 1){
		if(!is_array($function_to_add))
			$function_to_add = array(&$this, $function_to_add);
			
		return add_action( (string)$tag, $function_to_add, (int)$priority, (int)$accepted_args );
	}
	
	/* 
	 * Hook for add_filter()
	*/
	protected function add_filter($tag, $function_to_add, $priority = 10, $accepted_args = 1){
		if(!is_array($function_to_add))
			$function_to_add = array(&$this, $function_to_add);
			
		return add_filter( (string)$tag, $function_to_add, (int)$priority, (int)$accepted_args );
	}
	
	/* 
	 * Hook for remove_filter()
	*/
	protected function remove_filter($tag, $function_to_remove, $priority = 10){
		if(!is_array($function_to_remove))
			$function_to_remove = array(&$this, $function_to_remove);
			
		return remove_filter( (string)$tag, $function_to_remove, (int)$priority );
	}
}
endif;
new Carecru_Theme;