<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!"); ?>
<footer id="Footer">
<?php
	$widgets = array();
	for($i=1; $i<=5; ++$i)
	{
		if(is_dynamic_sidebar('footer-' . $i) && is_active_sidebar('footer-' . $i))
		{
			ob_start();
				dynamic_sidebar('footer-' . $i);
			$ob_get_clean = ob_get_clean();
			
			if(!empty($ob_get_clean))
				$widgets[]=$ob_get_clean;
		}
	}
	$widget_col = count($widgets);	
	if(!empty($widgets)):
?>
	<div class="widgets">
    	<div class="container">
        	<div class="row row-col-<?php echo $widget_col; ?>" data-col="<?php echo $widget_col; ?>">
            <?php foreach($widgets as $i => $sidebar) : ?>
            	<div class="col col-widget-<?php echo ($i+1); ?>"><?php echo $sidebar; ?></div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="copyright">
    	<div class="container text-center">
        	<?php if($footer_logo = get_theme_mod( 'footer_logo', 0 )) : ?><img src="<?php echo esc_url($footer_logo); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>" /><?php endif; ?> Copyright © <?php
            	$Y = date('Y');
				$C = 2018;
				echo $Y > $C ? $C . '-' . $Y : $Y;
			?> CareCru Inc. All rights reserved.
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<?php get_template_part( 'includes/mobile', 'menu' ); ?>
<?php get_template_part( 'includes/price', 'box' ); ?>
</body>
</html>