<div class="mobile-menu" id="MobileMenu" style="display: none;">
	<div class="mobile-menu-header">
	<?php
        $menu_logo_id = get_theme_mod( 'custom_logo' );
        if($menu_logo = wp_get_attachment_image_src( $menu_logo_id , 'full' ))
        {
            echo '<img src="' . esc_url( $menu_logo[0] ) . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '">';
        }
    ?>
    <button type="button" id="MobileMenuClose"><i class="fa fa-times"></i></button>
    </div>
	<div class="mobile-menu-container">
    	
        <?php
			if ( has_nav_menu( 'mobile-menu' ) ) {
				wp_nav_menu(array(
					'theme_location'	=> 'mobile-menu',
					'menu_class'		=> 'navbar-menu',
					'menu_id'			=> '',
					'echo'				=> true,
					'items_wrap'		=> '<ul class="%2$s">%3$s</ul>',
					'depth'				=> 1,
				));
			}
		?>
    </div>
</div>