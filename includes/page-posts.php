<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**
 * Post Page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 */
global $wp_query, $category, $current_parent, $current_category;

// We need pagination here
include CARECRU_INC . '/Pagination.php';

get_header();
?>
<section class="container-fluid" id="BlogPosts">
	<div class="container mt-2 mb-5">
    	<aside class="row">
            <div class="col-12 mobile">
                <?php if ( !(is_tag() || is_search() || isset($_REQUEST['s'])) ) get_template_part( 'includes/menu', 'sections' ); ?>
            </div>
            <aside class="col-lg-3">
                <?php get_template_part( 'includes/search', 'form' ); ?>
                <?php get_template_part( 'includes/menu', 'category' ); ?>
                <div class="desktop"><?php carecru_pagination('',2,$wp_query); ?></div>
            </aside>
            <div class="col-lg-9">
                <div class="desktop">
                    <?php if ( !(is_tag() || is_search() || isset($_REQUEST['s'])) ) get_template_part( 'includes/menu', 'sections' ); ?>
                </div>
                
                <?php
					$sticky = get_option( 'sticky_posts' );
					$args = array(
						'posts_per_page' => -1,
						'post__in'  => $sticky
                    );
                    
                    if($current_category > 0) $args['cat']=$current_category;

                    $sticky_query = new WP_Query( $args );
					if ( isset($sticky[0]) && $sticky_query->have_posts()): ?>
						<div id="StickyPosts" class="carousel slide mt-3 mb-3" data-ride="carousel">
                            <div class="carousel-inner">
                            <?php $i=0; while ( $sticky_query->have_posts() ) : $sticky_query->the_post(); ?>
                                <a href="<?php the_permalink(); ?>" class="carousel-item<?php echo $i===0 ? ' active' : ''; ?>">
                                	<?php echo get_the_post_thumbnail( get_the_ID(), 'large', array( 'class' => 'd-block w-100 img-fluid', 'alt' => esc_attr(get_the_title()) ) ); ?>
                                    <div class="carousel-caption">
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php echo wp_trim_words( get_the_content(), 55 ); ?></p>
                                        <div class="author d-none d-md-block"><?php echo get_avatar( get_the_author_meta( 'ID' ) , 32, CARECRU_IMG.'/avatar.jpg', esc_attr(get_the_author_meta('display_name')), array('class'=>'rounded-circle') ); ?> <?php echo get_the_author_meta('display_name'); ?></div>
                                    </div>
                                </a>
                            <?php ++$i; endwhile; wp_reset_postdata();?>
                            <?php if( $i > 1 ) : ?>
                                <a class="carousel-control-prev" href="#StickyPosts" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only"><?php _e('Previous', 'carecru'); ?></span>
                                </a>
                                <a class="carousel-control-next" href="#StickyPosts" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only"><?php _e('Next', 'carecru'); ?></span>
                                </a>
                            <?php endif; ?>
                            </div>
						</div>
				<?php endif; ?>
                
                <div class="row">
                <?php
                    $i = 1; if(have_posts()) :
					while ( have_posts() ) : the_post(); $article_id = get_the_ID(); 
                        get_template_part( 'includes/loop', 'posts' ); 
                    ++$i; endwhile; else: ?>
                    <h2 class="col-12 text-center"><?php _e('There is currently no content. Please visit us later.','carecru'); ?></h2>
                <?php endif; ?>
                </div>
                <div class="mobile"><?php carecru_pagination('',2,$wp_query); ?></div>
            </div>
    	</div>
    </div>
</section>
<?php get_footer();