<section class="popup-wrapper" style="display: none;" id="PopupGetPrice">
    <div class="popup-container">
        <div class="popup popup-step-1">
        	<button type="button" id="PopupGetPriceClose" class="PopupGetPriceClose"><i class="fa fa-times"></i></button>
            <div class="row popup-header">
                <div class="col-lg-9">
                    <h2>Get a Price</h2>
                    <p>Please take a second to fill out the form below and we'll be in touch shortly.</p>
                </div>
                <div class="col-lg-2">
                    <figure class="popup-header-image"><img src="<?php echo CARECRU_IMG; ?>/Donna-on-Laptop.png"></figure>
                </div>
            </div>                
            
            <div class="row">
                <div class="col-lg-12 popup-form">
                    
                    <?php echo do_shortcode('[contact-form-7 id="124" title="Get a Price"]'); ?>

                    <!-- <form role="form">
                        
                        <div class="row">
                        
                            <div class="col-lg-6">
                                <label for="first-name">First Name <span>*</span></label>
                                <input type="text" id="first-name">
                            </div>
                            
                            <div class="col-lg-6">
                                <label for="last-name">Last Name <span>*</span></label>
                                <input type="text" id="last-name">
                            </div>
                        
                        </div>
                
                        <div class="row">
                        
                            <div class="col-lg-6">
                                <label for="email">Email <span>*</span></label>
                                <input type="email" id="email">
                            </div>
                            
                            <div class="col-lg-6">
                                <label for="phone-number">Phone Number <span>*</span></label>
                                <input type="tel" id="phone-number">
                            </div>
                        
                        </div>
            
                        <div class="row">
                      
                            <div class="col-lg-6">
                                <label for="company-name">Comany Name <span>*</span></label>
                                <input type="text" id="company-name">
                            </div>
                            
                            <div class="col-lg-6">
                                <label for="practice-management-software">Practice Management Software <span>*</span></label>
                                <input type="text" id="practice-management-software">
                            </div>
                        
                        </div>
        
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit">Get a price</button>
                            </div>
                        </div>
        
                    </form> -->
                    
                </div>
                
            </div>
            
        </div>

        <div class="popup popup-step-2" style="display: none;">
        	<button type="button" id="PopupGetPriceClose" class="PopupGetPriceClose"><i class="fa fa-times"></i></button>
            <div class="row popup-header">
                <div class="col-lg-9">
                    <h2>Success!</h2>
                    <p>You have successfully submitted the price request. Our team member will get in touch with you shortly.</p>
                </div>
                <div class="col-lg-2">
                    <figure class="popup-header-image"><img src="<?php echo CARECRU_IMG; ?>/Donna-on-Laptop.png"></figure>
                </div>
            </div>                
            
            <div class="row">
                <div class="col-lg-12 popup-form text-center">
                    
                    <button type="button" class="PopupGetPriceClose">GOT IT</button>
                    
                </div>
                
            </div>
            
        </div>
    </div>
</section>