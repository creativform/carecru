<?php global $i, $article_id, $realated,  $post, $wp_query;
$format = get_post_format($article_id);
$class = 'col-sm-12 col-md-6 col-lg-4';
if(isset($realated) && $realated===true)
    $class = 'col-sm-12 col-md-6';

if($format == 'link') :
    $carecru_link = get_post_meta( isset( $post->ID ) ? $post->ID : (isset($wp_query->post) && isset($wp_query->post->ID) ? $wp_query->post->ID : 0), 'carecru_link', true );
    $carecru_link_target = get_post_meta( isset( $post->ID ) ? $post->ID : (isset($wp_query->post) && isset($wp_query->post->ID) ? $wp_query->post->ID : 0), 'carecru_link_target', true );
?>
    <article class="<?php echo $class; ?> mt-3 mb-3 article article-<?php echo $article_id; ?>" id="Article-ID-<?php echo $article_id; ?>">
        <a class="card" tabindex="<?php echo $i; ?>" href="<?php echo $carecru_link; ?>" target="<?php echo $carecru_link_target; ?>">
            <figure class="thumbnail"><?php echo get_the_post_thumbnail( $article_id, 'medium', array( 'class' => 'card-img-top img-fluid', 'alt' => esc_attr(get_the_title()) ) ); ?></figure>
            <div class="card-body">
                <h3 class="h3 card-title"><?php the_title(); ?></h3>
                <p class="card-text"><?php echo wp_trim_words( get_the_content(), 55 ); ?></p>
            </div>
        </a>
    </article>
<?php else : ?>
    <article class="<?php echo $class; ?> mt-3 mb-3 article article-<?php echo $article_id; ?>" id="Article-ID-<?php echo $article_id; ?>">
        <a class="card" tabindex="<?php echo $i; ?>" href="<?php the_permalink($article_id); ?>">
            <figure class="thumbnail"><?php echo get_the_post_thumbnail( $article_id, 'medium', array( 'class' => 'card-img-top img-fluid', 'alt' => esc_attr(get_the_title()) ) ); ?></figure>
            <div class="card-body">
                <h3 class="h3 card-title"><?php the_title(); ?></h3>
                <p class="card-text"><?php echo wp_trim_words( get_the_content(), 55 ); ?></p>
            </div>
        </a>
    </article>
<?php endif; ?>