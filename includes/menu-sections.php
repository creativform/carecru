<?php global $category, $current_parent, $current_category; ?>
<ul class="blog-submenu">
<?php if ( $blog_page = get_permalink( get_option( 'page_for_posts' ) ) ): ?>
    <li<?php
        if($current_category < 1 && !isset($_REQUEST['s']))  echo ' class="active"';
    ?>><a href="<?php echo $blog_page; ?>" alt="<?php echo esc_attr(__('All Resources','carecru')); ?>"><?php _e('All Resources','carecru'); ?></a></li>
<?php endif; ?>
<?php foreach($category[0] as $i=>$obj) : ?>
    <li<?php
        if($current_category === $obj->term_id || $current_parent === $obj->term_id) echo ' class="active"';
    ?>><a href="<?php echo esc_url( get_term_link( $obj->term_id ) ); ?>" alt="<?php echo esc_attr( $obj->name ); ?>"><?php echo $obj->name; ?></a></li>
<?php endforeach; ?>
</ul>