<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**********************************
 * Pagination
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 **********************************/
if(!function_exists('carecru_pagination')) :
	function carecru_pagination($pages = '', $range = 2, $wp_query = false) 
	{  
		
		global $paged;
		if(empty($paged)) $paged = 1;
		if($pages == '')
		{
			if(!$wp_query) global $wp_query;
			$pages = $wp_query->max_num_pages;
		
			if(!$pages)
				$pages = 1;		 
		}   
		$showitems = ($range * 2) + 1;
		
		if(1 != $pages)
		{
			echo '<nav aria-label="' . __('Page navigation', 'carecru') . '" role="navigation" class="mt-5">';
			echo '<span class="sr-only">' . __('Page navigation', 'carecru') . '</span>';
			echo '<ul class="pagination carecru-pagination">';
			
			if($paged > 1/* && $showitems < $pages*/) 
				echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($paged - 1).'" aria-label="' . __('Previous Page', 'carecru') . '"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-sm-down"> ' . __('Previous', 'carecru') . '</span></a></li>';
		
			for ($i=1; $i <= $pages; $i++)
			{
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
					echo ($paged == $i)? '<li class="page-item active"><span class="page-link"><span class="sr-only"' . __('Current Page', 'carecru') . '> </span>'.$i.'</span></li>' : '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($i).'"><span class="sr-only">' . __('Page', 'carecru') . ' </span>'.$i.'</a></li>';
			}
			
			if ($paged < $pages/* && $showitems < $pages*/) 
				echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($paged + 1).'" aria-label="' . __('Next Page', 'carecru') . '"><span class="hidden-sm-down">' . __('Next', 'carecru') . ' </span> <i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>';  

		
			echo '</ul>';
			echo '</nav>';	 	
		}
	}
endif;