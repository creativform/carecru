<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**
 * Include metaboxes
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 */
 
/**
* Calls the class on the post edit screen.
*/
function call_CareCru_Metabox() {
    new CareCru_Metabox();
}
 
if ( is_admin() ) {
    add_action( 'load-post.php',     'call_CareCru_Metabox' );
    add_action( 'load-post-new.php', 'call_CareCru_Metabox' );
}
 
/**
 * The Class.
 */
class CareCru_Metabox {
 
    /**
     * Hook into the appropriate actions when the class is constructed.
     */
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
        add_action( 'save_post',      array( $this, 'save'         ) );
    }
 
    /**
     * Adds the meta box container.
     */
    public function add_meta_box( $post_type ) {
        // Limit meta box to certain post types.
        $post_types = array( 'page', 'post' );
 
        if ( in_array( $post_type, $post_types ) ) {
            add_meta_box(
                'carecru_theme_setup',
                __( 'CareCru Theme Setup', 'carecru' ),
                array( $this, 'render_meta_box_content' ),
                $post_type,
                'advanced',
                'high'
            );
        }
    }
 
    /**
     * Save the meta when the post is saved.
     *
     * @param int $post_id The ID of the post being saved.
     */
    public function save( $post_id ) {
 
        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */
 
        // Check if our nonce is set.
        if ( ! isset( $_POST['carecru_setup_nonce'] ) ) {
            return $post_id;
        }
 
        $nonce = $_POST['carecru_setup_nonce'];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'carecru_setup' ) ) {
            return $post_id;
        }
 
        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }
 
        /* OK, it's safe for us to save the data now. */
 
        // Sanitize the user input.
        if(isset($_POST['carecru_header_type']))
            $carecru_header_type = sanitize_text_field( $_POST['carecru_header_type'] );

        $carecru_link = esc_html(sanitize_text_field( $_POST['carecru_link'] ));
        $carecru_link_target = sanitize_text_field( $_POST['carecru_link_target'] );
 
        // Update the meta field.
        if(isset($_POST['carecru_header_type']))
            update_post_meta( $post_id, 'carecru_header_type', $carecru_header_type );

        update_post_meta( $post_id, 'carecru_link', $carecru_link );
        update_post_meta( $post_id, 'carecru_link_target', $carecru_link_target );
    }
 
 
    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_content( $post ) {
        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'carecru_setup', 'carecru_setup_nonce' );
 
        // Use get_post_meta to retrieve an existing value from the database.
        if($post->post_type=='page') $carecru_header_type = get_post_meta( $post->ID, 'carecru_header_type', true );
        $carecru_link = get_post_meta( $post->ID, 'carecru_link', true );
        $carecru_link_target = get_post_meta( $post->ID, 'carecru_link_target', true );
 
        // Display the form, using the current value.
        ?>
        <?php if($post->post_type=='page') : ?>
        <div>
            <label for="header_type">
                <?php _e( 'Header type', 'carecru' ); ?>: 
            </label><br>
            <select id="carecru_header_type" name="carecru_header_type">
            <?php
                foreach(array(
                    'standard' => __( 'Standard', 'carecru' ),
                    'sticky' => __( 'Sticky', 'carecru' ),
                    'sticky-white' => __( 'Sticky White', 'carecru' )
                ) as $key => $name){
                    
                    $selected='';
                    if(empty($carecru_header_type) && $key == 'standard')
                        $selected=' selected';
                    else if($carecru_header_type == $key) $selected=' selected';
                    
                    printf('<option value="%1$s"%3$s>%2$s</option>', $key, $name, $selected);	
                }
            ?>
            </select>
        </div><br>
        <?php endif; ?>
        <div>
            <label for="carecru_link">
                <?php _e( 'URL (for link format)', 'carecru' ); ?>: 
            </label><br>
            <input type="url" id="carecru_link" name="carecru_link" value="<?php echo $carecru_link; ?>" style="width:100%; max-width:400px" />
        </div><br>
        <div>
            <label for="carecru_link_target">
                <?php _e( 'Open url in', 'carecru' ); ?>: 
            </label><br>
            <select id="carecru_link_target" name="carecru_link_target">
            <?php
                foreach(array(
                    '_self' => __( 'Same window', 'carecru' ),
                    '_blank' => __( 'New window', 'carecru' )
                ) as $key => $name){
                    
                    $selected='';
                    if(empty($carecru_link_target) && $key == 'standard')
                        $selected=' selected';
                    else if($carecru_link_target == $key) $selected=' selected';
                    
                    printf('<option value="%1$s"%3$s>%2$s</option>', $key, $name, $selected);	
                }
            ?>
            </select>
        </div>
        <?php
    }
}