<?php if (!defined('WPINC') || !defined('ABSPATH')) die("Don't try to trick us. We know who you are!");
/**
 * FAQ Page
 *
 * @package WordPress
 * @subpackage carecru
 * @since 0.0.1
 * @version 0.0.1
 * @author Ivijan-Stefan Stipic
 * @url https://infinitumform.com
 */
get_header(); ?>
<?php
$accordion = new WP_Query( array(
	'post_type' => 'faq',
	'posts_per_page' => -1,
	'tax_query' => array(
		'taxonomy' => 'faq-category',
		'field' => 'slug',
		'terms' => 'landing-page'
	)
) );
if($accordion->have_posts()) :
?>
<section class="container-fluid" id="FAQ">
	<div class="container pt-5 pb-5">
    	<div class="row align-items-center justify-content-center">
            <h1 class="col-12 mb-5 section-title text-center">
            	Frequently Asked Questions
            </h1>
            <div class="col-12">
            	<div class="accordion" id="AccordionFAQ">
				<?php
                    $i=-999999;
                    while ( $accordion->have_posts() ) : $accordion->the_post();
                ?>
                
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link<?php echo $i===0 ? '' : ' collapsed'; ?>" type="button" data-toggle="collapse" data-target="#FAQ-<?php the_ID(); ?>" aria-expanded="<?php echo $i===0 ? 'true' : 'false'; ?>" aria-controls="FAQ-<?php the_ID(); ?>">
                                    <?php the_title(); ?>
                                </button>
                            </h5>
                        </div>
                        
                        <div id="FAQ-<?php the_ID(); ?>" class="collapse<?php echo $i===0 ? ' show' : ''; ?>" aria-labelledby="FAQ-<?php the_ID(); ?>" data-parent="#AccordionFAQ">
                            <div class="card-body">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <?php ++$i; endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php get_footer();