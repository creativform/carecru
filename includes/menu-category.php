<?php
global $category, $current_parent, $current_category;

/**
 * Huh, let's solve this problem
*/

$cat_obj = array();

$all_cat = $current_parent > 0 ? get_term_link( $current_parent ) : true;
if ( $all_cat === true)
{
    if($current_category === 0)
    {
        /**
         * Let's list all subcategories when there is no parents selected
        */
    /*    if ( $blog_page = get_permalink( get_option( 'page_for_posts' ) ) )
        {
            $cat_obj[]=(object)array(
                'id'=>0,
                'name'=>__('All','carecru'),
                'url'=>esc_url( $blog_page ),
                'active'=>($current_parent < 1 && !isset($_REQUEST['s']))
            );
        }

        foreach($category as $x=>$o)
        {
            if($x===0) continue;

            foreach($o as $i=>$obj)
            {
                $cat_obj[]=(object)array(
                    'id'=>$obj->term_id,
                    'name'=>$obj->name,
                    'url'=>esc_url( get_term_link( $obj->term_id ) ),
                    'active'=>($current_category === $obj->term_id)
                );
            }
        }
        */
    }
    elseif(isset($category[$current_category]))
    {
        /**
         * Now we have parent setup, let's list childs
        */
        $cat_obj[]=(object)array(
            'id'=>0,
            'name'=>__('All','carecru'),
            'url'=>esc_url( get_term_link( $current_category ) ),
            'active'=>($current_parent < 1 && !isset($_REQUEST['s']))
        );

        foreach($category[$current_category] as $i=>$obj)
        {
            $cat_obj[]=(object)array(
                'id'=>$obj->term_id,
                'name'=>$obj->name,
                'url'=>esc_url( get_term_link( $obj->term_id ) ),
                'active'=>($current_category === $obj->term_id)
            );
        }
    }
}
else
{
    /**
     * Now is funny moment, we need categories from parent
    */
    $subcategory_array = get_terms( array(
        'taxonomy' => 'category',
        'hide_empty' => false,
        'child_of' => ($current_parent > 0 ? $current_parent : $current_category),
    ) );

    $cat_obj[]=(object)array(
        'id'=>0,
        'name'=>__('All','carecru'),
        'url'=>esc_url( $all_cat ),
        'active'=>($current_category === $current_parent && !isset($_REQUEST['s']))
    );

    foreach($subcategory_array as $i=>$obj)
    {
        $cat_obj[]=(object)array(
            'id'=>$obj->term_id,
            'name'=>$obj->name,
            'url'=>esc_url( get_term_link( $obj->term_id ) ),
            'active'=>($current_category === $obj->term_id)
        );
    }
}

/**
 * Print all this on the proper way
*/
?>
<ul class="category-menu desktop">
<?php foreach($cat_obj as $i=>$obj) : ?>
    <li<?php
        if($obj->active) echo ' class="active"';
    ?>><a href="<?php echo $obj->url; ?>" alt="<?php echo esc_attr( $obj->name ); ?>"><?php echo $obj->name; ?></a></li>
<?php endforeach; ?>
</ul>
<select onchange="window.location.href = this.options[this.selectedIndex].value;" class="form-control category-menu-select mobile">
    <?php foreach($cat_obj as $i=>$obj) : ?>
        <option<?php
            if($obj->active) echo ' selected';
        ?> value="<?php echo $obj->url; ?>"><?php echo $obj->name; ?></option>
    <?php endforeach; ?>
</select>​
